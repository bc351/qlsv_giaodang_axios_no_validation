// call api
var dssv = [];
const base_url = "https://633ec08883f50e9ba3b76613.mockapi.io"
var batLoading = function(){
    document.getElementById('loading').style.display = 'flex'
}
var tatLoading = function(){
    document.getElementById('loading').style.display = 'none'
}
var fetchDssvService = function(){
batLoading()
    axios({
    url: `${base_url}/sv`,
    method: "get",
})
.then(function(res){
    console.log('res: ', res.data);
    tatLoading()
    // document.getElementById('txtMaSV').disabled = false
    var dssv = res.data.map(function(sv){
        return new sinhVien(
        sv.ma,
        sv.ten,
        sv.email,
        sv.matKhau, 
        sv.ly,
        sv.toan,
        sv.hoa,
     
        )
        
    })
    renderDssv(dssv);
    console.log('dssv: ', dssv);
    

})
.catch(function(err){
    console.log('err: ', err);
    tatLoading()

})
}
fetchDssvService()

var renderDssv = function(listSv){
    var contentHTML = "";
    listSv.forEach(function(sv){
        contentHTML += `<tr>
                                        <td id="ma">${sv.ma}</td>
                                        <td>${sv.ten}</td>
                                        <td>${sv.email}</td>
                                         <td>${sv.tinhDTB()}</td>
                                        <td><button onclick="layThongTinChiTiet(${sv.ma})"  class="btn btn-primary">Sua </button></td>
                                        <td><button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xoa </button></td>
                                        </tr>`
    })

    document.getElementById('tbodySinhVien').innerHTML = contentHTML
}
// khi xoa phai truyen vao idSv tren tung button duoc create ra boi ham renderDssv()
var xoaSv = function(idSv){
    // goi APIs de xoa
    batLoading()
    axios({
        url: `${base_url}/sv/${idSv}`,
        method: 'DELETE',
    })
    .then(function(res){
        tatLoading()
        Swal.fire('Deleted successfully')
    
        console.log('res: ', res.data);
        fetchDssvService(res.data)
    })
    .catch(function(err){
        tatLoading()
        console.log('err: ', err);
        Swal.fire('Fail to delete')
    })
}
var themSv = function(){
    batLoading()
    var sv = layThongTinTuForm()
    console.log('sv: ', sv);
    
    axios({
        url: `${base_url}/sv`,
        method: "POST",
       data: sv,
    })
    .then(function(res){
        tatLoading()
        Swal.fire('Added successfully')
        console.log('res: ', res.data);
        fetchDssvService()
        reset()

    })
    .catch(function(err){
        tatLoading()
        Swal.fire('Failed to add')
        console.log('err: ', err);
        reset()
    })
}
function layThongTinChiTiet(idSv){
// goi axios lay thong tin
axios({
    url: `${base_url}/sv/${idSv}`,
    method: 'get',
})
.then(function(res){
    console.log('res: ', res);
    document.getElementById('txtMaSV').disabled = true
    showThongTinLenForm(res.data)
})

.catch(function(err){
    console.log('err: ', err);

})
}
// var maSv = document.getElementById('ma').value 
function capNhatSv(){
    batLoading()
    var svEdit = layThongTinTuForm()
    var idSv = svEdit.ma;
    axios({
    url: `${base_url}/sv/${idSv}`,
    method: 'PUT',
    data: svEdit
    })
    .then(function(res){
        tatLoading()
        Swal.fire('Upgraded successfully')
        document.getElementById('txtMaSV').disabled = false
        console.log('res: ', res.data);
        fetchDssvService(res.data)
       
        reset()
    })
    .catch(function(err){
        tatLoading()
        Swal.fire('Fail to upgrade')
        console.log('err: ', err);

    })
}



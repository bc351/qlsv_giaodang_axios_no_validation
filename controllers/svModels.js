let sinhVien = function(ma,ten,email,matKhau,ly,toan,hoa){
    this.ma = ma, 
    this.ten = ten,
    this.email = email,
    this.matKhau = matKhau,
    this.ly = ly,
    this.toan= toan,
    this.hoa= hoa,
    this.tinhDTB = function(){
       var dtb = (this.ly*1+this.toan*1+this.hoa*1)/3;
       return dtb.toFixed(1)
    }
    
}